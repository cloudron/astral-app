# Astral Cloudron App

This repository contains the Cloudron app package source for [astral](https://github.com/astralapp/astral).

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd astral-app

cloudron build
cloudron install
```

## Notes

* Astral requires a Github Oauth Application. You are able to create one from you Github account and then adjust the `.env` file to use this application: 
  - Go to [Github -> Settings -> OAuth Apps](https://github.com/settings/developers)
  - Create a new OAuth app
  - Enter `Astral`, `astral.yourcloudron.com`, and `https://astral.yourcloudron.com/auth/github/callback` for Application Name, Homepage URL, and Authorization callback URL, respectively. 
  - Generate a new Client Secret, make note of this, you'll only see it once. 
  - On your Astral app instance, edit the `/app/data/.env` file. Update the `GITHUB_CLIENT_ID` and `GITHUB_CLIENT_SECRET` values with your Github OAuth App's Client ID and Client Secret. 
  - Restart the app
  - Login with Github!
