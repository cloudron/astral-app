#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = webdriver.By,
    Keys = webdriver.Key,
    until = webdriver.until,
    chrome = require('selenium-webdriver/chrome'),
    Builder = require('selenium-webdriver').Builder;

describe('Application life cycle test', function () {
    this.timeout(0);

    let browser;
    var LOCATION = 'test';
    var app;
    var TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 5000;

    let options = new chrome.Options();
    options.addArguments("user-data-dir=tmp/foo");
    options.windowSize({ width: 1280, height: 1024 });

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        if (!process.env.GITHUB_CLIENT_ID) return done(new Error('GITHUB_CLIENT_ID env var not set'));
        if (!process.env.GITHUB_CLIENT_SECRET) return done(new Error('GITHUB_CLIENT_SECRET env var not set'));

        browser = new Builder().forBrowser('chrome').setChromeOptions(options).build();

        done();
    });

    after(function (done) {
        if (browser) browser.quit();
        done();
    });

    function setupOauthCreds(done) {
        const CLIENT_ID = process.env.GITHUB_CLIENT_ID;
        const CLIENT_SECRET = process.env.GITHUB_CLIENT_SECRET;

        const cmd = `cloudron exec --app ${app.fqdn} -- bash -c 'sed -i -e "s#GITHUB_CLIENT_ID=.*#GITHUB_CLIENT_ID=${CLIENT_ID}#" -e "s#GITHUB_CLIENT_SECRET=.*#GITHUB_CLIENT_SECRET=${CLIENT_SECRET}#" /app/data/env'`;
        execSync(cmd, { encoding: 'utf8', stdio: 'inherit' });

        done();
    }

    function userLogin(done) {
        browser.get('https://' + app.fqdn).then(function () {
            console.log("Please login via Github");
            return browser.sleep(30000);
        }).then(function () {
            done();
        });
    }

    function createTestTag(done) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//span[contains(text(),"Add a tag")]'))).click();
        }).then(function () {
            return browser.findElement(by.xpath('//input[contains(@placeholder,"Enter a tag name")]'));
        }).then(function (tagInput) {
            return tagInput.sendKeys('ThisTagExistsForTheCloudronTest');
        }).then(function() {
            return browser.findElement(by.xpath('//input[contains(@placeholder,"Enter a tag name")]'));
        }).then(function(tagInput) {
            return tagInput.sendKeys(Keys.ENTER);
        }).then(function () {
            return browser.sleep(5000);
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//span[contains(text(),"ThisTagExistsForTheCloudronTest")]')), TIMEOUT);
        }).then(function () {
            done();
        });
    };

    function checkTestTagExists(done) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//span[contains(text(),"ThisTagExistsForTheCloudronTest")]')), TIMEOUT);
        }).then(function () {
            done();
        });
    };

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can properly setup Github oauth', setupOauthCreds);

    it('can login', userLogin);
    
    it('can create a tag', createTestTag);
    
    it('can check test tag exists', checkTestTagExists);

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        done();
    });

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --app ${app.id} --raw`));
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can check test tag exists', checkTestTagExists);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install previous version from appstore', function () {
        execSync('cloudron install --appstore-id com.astralapp.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('can properly setup Github oauth', setupOauthCreds);

    it('can login', userLogin);
    
    it('can create a tag', createTestTag);
    
    it('can check test tag exists', checkTestTagExists);

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', userLogin);

    it('can check test tag exists', checkTestTagExists);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
