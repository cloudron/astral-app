FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

ARG RELEASE=6f5b706f91ee82e9b4119c680442508dad529aec

RUN mkdir -p /app/code 
WORKDIR /app/code

COPY start.sh env.production /app/pkg/

RUN \
# Install Astral
    curl -Ls https://github.com/astralapp/astral/archive/${RELEASE}.tar.gz | tar -xzf - --strip 1 -C /app/code \
    && composer install \
    && yarn \
    && php artisan astral:install \
    && yarn prod \
    && ln -sf /app/data/env /app/code/.env \
    && chown -R www-data.www-data /app/code

RUN mv /app/code/storage /app/code/storage.original && \
    ln -s /app/data/storage /app/code/storage

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
RUN a2enmod rewrite
COPY apache/astral.conf /etc/apache2/sites-enabled/astral.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

# Configure mod_php
RUN a2enmod php7.4 headers

CMD [ "/app/pkg/start.sh" ]
