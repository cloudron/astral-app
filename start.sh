#!/bin/bash

set -eu

mkdir -p /app/data /app/pkg /app/data/storage /app/data/storage/logs /app/data/storage/app /app/data/storage/app/public /app/data/storage/framework /app/data/storage/framework/cache /app/data/storage/framework/cache/data /app/data/storage/framework/sessions /app/data/storage/framework/testing /app/data/storage/framework/views

echo "==> Creating config"
if [[ ! -f /app/data/.initialized ]]; then
  echo "==> Initializing"
  cp /app/pkg/env.production /app/data/env
  php artisan key:generate

  touch /app/data/.initialized
fi

echo "==> Migrating DB"
php artisan migrate

echo "==> Changing permissions"
chmod 777 -R /app/data/storage
chown -R www-data:www-data /app/data/

echo "==> Staring Astral"

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
