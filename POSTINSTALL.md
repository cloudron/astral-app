Astral requires a Github OAuth Application to login. See the [setup instructions](https://docs.cloudron.io/apps/astral/#setup)
to use the app.

